# SOME DESCRIPTIVE TITLE.
# Copyright (C) Jean Abou Samra, 2021-2023. Free to reuse, CC0-licensed
# This file is distributed under the same license as the Python package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version:  Python\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-26 23:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.12.1\n"

msgid "Messages"
msgstr ""

msgid "Printing text in the console is essential to be able to debug your code. There are several ways to do that."
msgstr ""

msgid "The `display` function prints any object (be it a string or not). Use it several times to print several objects. `display` does not insert any newlines automatically; you need to add a newline at the end of the string (if you are displaying a string), or to add a call `(display \"\\n\")`, which can be shortened as `(newline)`. For example, this code:"
msgstr ""

msgid ""
"(define surname \"Salima\")\n"
"(display \"Hello \")\n"
"(display surname)\n"
"(display \"!\\n\")\n"
msgstr ""

msgid "can also be written"
msgstr ""

msgid ""
"(define surname \"Salima\")\n"
"(display \"Hello \")\n"
"(display surname)\n"
"(display \" !\")\n"
"(newline)\n"
msgstr ""

msgid "Both of the examples above were somewhat tedious. It is more convenient to use the `format` procedure. To begin, we are going to use `#t` as its first argument (read on to understand what this object is). The second argument is a string, which can contain special sequences. There must be the same number of remaining arguments as special sequences. They are formatted in a certain way depending on which special sequence is used, and inserted in the string. The formatted string is then displayed on the console. One of the commonly used escape sequences is `~a`, which prints objects exactly like `display` would."
msgstr ""

msgid ""
"(define surname \"Salima\")\n"
"(format #t \"Hello ~a!\\n\" surname)\n"
"⊨ Hello Salima!\n"
"⇒ #t\n"
msgstr ""

msgid "With `#t`, `format` returns an unimportant value. On the other hand, if `#f` is used, `format` returns the formatted string, without displaying it:"
msgstr ""

msgid ""
"(define surname \"Salima\")\n"
"(format #f \"Hello ~a!\" surname)\n"
"⇒ \"Hello Salima!\"\n"
msgstr ""

msgid "As an example, LilyPond extensively uses `format` with `#f` to build its PostScript output. The following code excerpt is taken from [framework-ps.scm](https://gitlab.com/lilypond/lilypond/-/blob/master/scm/)."
msgstr ""

#, python-format
msgid ""
"(format #f \"%%BeginDocument: ~a\n"
"~a\n"
"%%EndDocument\n"
"\"\n"
"          file-name\n"
"          (cached-file-contents file-name))\n"
msgstr ""

msgid "There is another procedure, which is defined by LilyPond, called `ly:message`. It works much like `format`. Its only purpose is to display the messages, so it does not take the first argument `#t` or `#f`; it is as if `#t` were used. Its advantage is that it prints the message in the standard error stream, not in the standard output stream, which prevents it from being intermingled with the rest of logging messages in the Frescobaldi interface (or another interface). (Actually, it would be possible to replace `#t` with `(current-error-port)` in `format` to get the same effect.) Moreover, `ly:message` automatically prepends a newline."
msgstr ""

msgid ""
"(define file \"emmentaler-20.otf\")\n"
"(ly:message \"Reading ~a...\" file)\n"
"⊨ Reading emmentaler-20.otf\n"
msgstr ""

msgid "Another special sequence is `~s`. It formats values like the sandbox would do. For example, it prints quotes around strings. `~s` is more suitable for debugging than `~a`."
msgstr ""

msgid ""
"(define file \"emmentaler-20.otf\")\n"
"(ly:message \"Reading ~s...\" file)\n"
"⊨ Reading \"emmentaler-20.otf\"\n"
msgstr ""

