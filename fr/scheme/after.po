# SOME DESCRIPTIVE TITLE.
# Copyright (C) Jean Abou Samra, 2021-2022. Contenu libre placé sous license
# CC0
# This file is distributed under the same license as the Tutoriel Scheme
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Tutoriel Scheme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-02 09:51+0200\n"
"PO-Revision-Date: 2023-06-02 09:51+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.13.0\n"

msgid "Appendix: Other Scheme resources"
msgstr "Annexe: Autres ressources sur Scheme"

msgid "Here are a few resources to learn more about the Scheme language."
msgstr "Voici quelques ressources qui vous permettront d'en apprendre plus sur Scheme."

msgid "[LilyPond's Scheme](https://scheme-book.readthedocs.io/), a tutorial that is similar to this one. It is unfinished, but some subjects are treated in more detail. (It is available in English only, while this one has a French version.)"
msgstr "[LilyPond's Scheme](https://scheme-book.readthedocs.io/), un tutoriel semblable à celui-ci, en anglais, fait par Urs Liska.  Il n'est pas complet, mais traite de certains sujets plus en détail que ce tutoriel-ci."

msgid "[Guile's reference manual](https://www.gnu.org/software/guile/docs/docs-2.2/guile-ref/) is a vital aid for ambitious projects. Remember to read the manual for the right version; this link is for version 2.2, which is the one used in standard binaries of LilyPond 2.24."
msgstr "Le [manuel de Guile](https://www.gnu.org/software/guile/docs/docs-2.2/guile-ref) est une référence indispensable pour les projets ambitieux. Attention à la prendre la version; ce lien pointe vers le manuel de Guile 2.2, la version normalement distribuée avec LilyPond 2.24."

msgid "Numerous tutorials and books have been written about Scheme, with basic to advanced content (and focusing on the language in general, not in the context of LilyPond like this tutorial). Two of these books deserve special mention: [Structure and Interpretation of Computer Programs](https://web.mit.edu/6.001/6.037/sicp.pdf) (by Abelson and Sussman, considered as a Bible by many Schemers), and [An Introduction to Scheme and its Implementation](https://www.cs.utexas.edu/ftp/garbage/submit/notready/schintro.ps) (by Paul Wilson). Many more can be found on [this page](https://schemers.org/Documents/#intro-texts) from https://schemers.org. The same website also suggests [How to Design Programs](https://htdp.org) (Felleisen, Findler, Flatt and Krishnamurti)."
msgstr "Il existe une multitude de tutoriels et livres sur Scheme, plus ou moins avancés (et qui se concentrent sur le langage en général sans mentionner les aspects de son interaction avec LilyPond, comme le fait ce tutoriel).  Citons notamment deux livres, relativement anciens mais toujours pertinents: [Structure and Interpretation of Computer Programs](https://web.mit.edu/6.001/6.037/sicp.pdf) (d'Abelson et Sussman, considéré comme la Bible par bien des Schemeurs), et [An Introduction to Scheme and its Implementation](https://www.cs.utexas.edu/ftp/garbage/submit/notready/schintro.ps) (de Paul Wilson).  Vous trouverez bien d'autres livres sur [cette page](https://schemers.org/Documents/#intro-texts) du site https://schemers.org.  Le même site propose également de lire [How to design Programs](https://htdp.org/) (Felleisen, Findler, Flatt et Krishnamurthi)."

