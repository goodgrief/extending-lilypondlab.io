"""Inject variables into Jinja context for the language selector."""

from pathlib import Path

from babel import Locale

langs = Path("LINGUAS").read_text(encoding="utf-8").splitlines()

# en -> English, fr -> Français, etc.
lang_names = {code: Locale(code).get_display_name().capitalize() for code in langs}


def setup(app):
    def inject_lang_context(app, pagename, templatename, context, doctree):
        # This is used for the language links at the top of each page.
        current_lang = app.config.language
        context["current_lang"] = lang_names[current_lang]
        context["lang_links"] = [
            {"code": code, "name": name}
            for code, name in lang_names.items()
            if code != current_lang
        ]

    app.connect("html-page-context", inject_lang_context)
