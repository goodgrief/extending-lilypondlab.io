import re

from sphinx.transforms import SphinxTransform
from docutils.nodes import Text, TextElement
from sphinx.util.nodes import is_smartquotable


class FrenchTypography(SphinxTransform):
    narrow_nbsp = "\u202f"
    replacements = {
        r":(?!//|\w)": narrow_nbsp + ":",
        ";": narrow_nbsp + ";",
        "[?]": narrow_nbsp + "?",
        "!": narrow_nbsp + "!",
    }

    default_priority = 750

    def apply(self, **kwargs):
        for node in self.document.traverse(TextElement):
            for txtnode in node.traverse(Text):
                if is_smartquotable(txtnode):
                    newtext = txtnode
                    for source, replacement in self.replacements.items():
                        newtext = re.sub(source, replacement, newtext)
                    txtnode.parent.replace(txtnode, Text(newtext))


def setup(app):
    if app.config.language == "fr":
        app.add_transform(FrenchTypography)
