from pygments.lexer import bygroups, inherit
from pygments.token import Generic
from pygments.lexers import SchemeLexer


class SchemeResultLexer(SchemeLexer):
    name = "Scheme with result markers"
    aliases = ["scheme-result"]
    tokens = {
        "value": [
            ("⇒", Generic.Output.Marker),
            ("(⊨)([^\n]*)", bygroups(Generic.Output.Marker, Generic.Output)),
            inherit,
        ],
    }
